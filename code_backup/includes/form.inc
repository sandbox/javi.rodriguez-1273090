<?php

function code_backup_admin ()
{
  //print 
  //$cron = variable_get();
  $path = variable_get('path_code_backup', FALSE);

  if (empty($path[nodes])) {
	$path[nodes] = getcwd();
  }
  if (empty($path[panels])) {
	$path[panels] = getcwd();
  }
  if (empty($path[blocks])) {
	$path[panels] = getcwd();
  }

  $config = variable_get('vars_code_backup',FALSE);

  $form['panels'] = array(
    '#type' => 'checkbox', 
	'#title' => t('Panels'), 
	'#default_value' => $config[panels], 
  );
  $form['panels_path'] = array (
    '#type' => 'textfield', 
    '#title' => t('Path'), 
    '#default_value' => $path[panels], 
    '#maxlength' => 255,
    '#required' => TRUE,
    '#description' => t('Carpeta para os paneis'),

  );

  $form['nodes'] = array(
    '#type' => 'checkbox', 
	'#title' => t('Nodes'), 
	'#default_value' => $config[nodes], 
  );  
  $form['nodes_path'] = array (
    '#type' => 'textfield', 
    '#title' => t('Path'), 
    '#default_value' => $path[nodes], 
    '#maxlength' => 255,
    '#required' => TRUE,
    '#description' => t('Carpeta para os nodos').t('-HAZLO CON JAVASCRIPT! aqui molaba q se activara o desactivar asi esta activado o no el check-')
  );
  
  $form['blocks'] = array(
    '#type' => 'checkbox', 
	'#title' => t('Blocks'), 
	'#default_value' => $config[blocks], 
  );  
  $form['blocks_path'] = array (
    '#type' => 'textfield', 
    '#title' => t('Path'), 
    '#default_value' => $path[blocks], 
    '#maxlength' => 255,
    //'#required' => TRUE,
    '#description' => t('Carpeta para os bloques')
  );
  
  $form['cron'] = array(
    '#type' => 'checkbox', 
	'#title' => t('Execute with cron'), 
	'#default_value' => $config[cron], 
	'#maxlength' => 255
  );
  $form['submit'] = array(
    '#type'  => 'submit',
    '#value' => t('Go!'),
  );
  $form['config'] = array(
    '#type'  => 'submit',
    '#value' => t('Save configuration'),
  );
  $form['#submit'][] ='code_backup_do';
  /** 
  * Activiar/Desactivar path
  */
  /*
  if ($config[panels] == 0) {
	 $form['panels_path']['#disabled'] = TRUE;
  }*/
  return $form;
}
?>